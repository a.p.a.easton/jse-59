package ru.nlmk.study.jse59;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.nlmk.study.jse59.entity.*;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class FindQuery {

    public List<Vehicle> findByYearNotOlderThanAndTypes(Date notOlderThan,
                                                        List<Class<? extends Vehicle>> types,
                                                        SessionFactory sessionFactory){

        Session session = sessionFactory.getCurrentSession();

        session.beginTransaction();

        Query query = session.createQuery("SELECT v FROM Vehicle v WHERE v.year >= :notOlderThan");
        query.setParameter("notOlderThan", notOlderThan);
        List<Vehicle> list = query.getResultList();
        session.getTransaction().commit();
        session.close();
        return list;
    }

}
