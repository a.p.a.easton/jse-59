package ru.nlmk.study.jse59;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.nlmk.study.jse59.entity.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .addAnnotatedClass(Vehicle.class)
                .addAnnotatedClass(SportCar.class)
                .addAnnotatedClass(ElectricCar.class)
                .addAnnotatedClass(Truck.class)
                .addAnnotatedClass(Bus.class)
                .buildSessionFactory();

        List<Class<? extends Vehicle>> types = Arrays.asList(SportCar.class, ElectricCar.class);

        FindQuery findQuery = new FindQuery();
        List<Vehicle> byYearNotOlderThanAndTypes = findQuery.findByYearNotOlderThanAndTypes(new Date(2021, 04, 28), types, factory);
        System.out.println(byYearNotOlderThanAndTypes);


        /*Vehicle vehicle = new Vehicle();
        vehicle.setBrand("BMW");
        vehicle.setModel("C");
        vehicle.setYear(new Date(2021,04,29));

        SportCar sportCar = new SportCar();
        sportCar.setHorsePower(20);
        sportCar.setAcceleration(5);
        sportCar.setBrand("Audi");
        sportCar.setModel("A6");

        ElectricCar electricCar = new ElectricCar();
        electricCar.setMaxDistance(50);

        Truck truck = new Truck();
        truck.setCarrying(40);

        Bus bus = new Bus();
        bus.setMaxPassengers(7);

        Session session;

        session = factory.getCurrentSession();

        session.beginTransaction();

        session.save(vehicle);
        session.save(sportCar);
        session.save(electricCar);
        session.save(truck);
        session.save(bus);

        session.getTransaction().commit();

        session.close();

        session = factory.getCurrentSession();

        session.beginTransaction();

        SportCar sport = session.get(SportCar.class, 19L);

        session.getTransaction().commit();

        session.close();*/

        //System.out.println(sport);


    }

}
