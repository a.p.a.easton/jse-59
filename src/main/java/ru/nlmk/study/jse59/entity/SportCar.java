package ru.nlmk.study.jse59.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table
public class SportCar extends Vehicle{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "horsepower")
    int horsePower;

    @Column(name = "acceleration")
    int acceleration;

    @Override
    public String toString() {
        return "SportCar{" +
                "id=" + id +
                ", horsePower=" + horsePower +
                ", acceleration=" + acceleration +
                '}';
    }
}
